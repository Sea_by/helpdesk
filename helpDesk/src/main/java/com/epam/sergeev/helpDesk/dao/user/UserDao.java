package com.epam.sergeev.helpDesk.dao.user;

import com.epam.sergeev.helpDesk.dao.CRUDdao;
import com.epam.sergeev.helpDesk.model.User;

public interface UserDao extends CRUDdao<User> {

    public User getByLogin(String login);

    public User getByEmail(String email);

}
