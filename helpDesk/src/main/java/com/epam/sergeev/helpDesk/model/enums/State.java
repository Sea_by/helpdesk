package com.epam.sergeev.helpDesk.model.enums;

public enum State {
    Draft,
    New,
    Approved,
    Decline,
    In_Progress,
    Done,
    Canceled;
}
