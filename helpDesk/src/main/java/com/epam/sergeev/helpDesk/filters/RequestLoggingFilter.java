package com.epam.sergeev.helpDesk.filters;

import java.io.IOException;
import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

@WebFilter(urlPatterns = "/*", dispatcherTypes = DispatcherType.REQUEST)
public class RequestLoggingFilter implements Filter {

    private static final Logger LOG = LoggerFactory.getLogger(RequestLoggingFilter.class);

    @Override
    public void init(FilterConfig fConfig) throws ServletException {
        LOG.info("RequestLoggingFilter initialized");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;

        if (req.getUserPrincipal() != null) {
            LOG.info("User: " + req.getUserPrincipal().getName() + "go to" + req.getRequestURI());
        } else {
            LOG.info("anonymousUser go to" + req.getRequestURI());
        }

        Cookie[] cookies = req.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                LOG.info(req.getRemoteAddr() + " " + cookie.getName() + " " + cookie.getValue());
            }
        }
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
    }
}
