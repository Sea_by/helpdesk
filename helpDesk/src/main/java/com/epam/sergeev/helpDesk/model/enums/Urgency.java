package com.epam.sergeev.helpDesk.model.enums;

public enum Urgency {
    Critical,
    High,
    Average,
    Low;
}
