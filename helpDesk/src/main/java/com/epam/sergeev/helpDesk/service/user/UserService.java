package com.epam.sergeev.helpDesk.service.user;

import com.epam.sergeev.helpDesk.model.User;
import java.util.List;

public interface UserService {

    List<User> getAll();

    User getUserByEmail(String email);

    void create(User user);

    String getUserEmail();
}
