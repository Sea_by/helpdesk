package com.epam.sergeev.helpDesk.dao.ticket;

import com.epam.sergeev.helpDesk.dao.CRUDdao;
import com.epam.sergeev.helpDesk.model.Ticket;

public interface TicketDao extends CRUDdao<Ticket> {

}
