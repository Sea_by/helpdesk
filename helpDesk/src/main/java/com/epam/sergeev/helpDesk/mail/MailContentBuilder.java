package com.epam.sergeev.helpDesk.mail;

import java.util.List;
import javafx.util.Pair;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;

@Service
public class MailContentBuilder {

    private static final String VIEW_NAME = "mail/ticketMail";

    private SpringTemplateEngine templateEngine = new SpringTemplateEngine();

    public String build(String name, List<Pair> byTicket, double total) {
        Context context = new Context();
        String ticket = null;
        for (Pair item : byTicket) {
            ticket += item.getKey() + " " + item.getValue() + "\n";
        }
        context.setVariable("name", name);
        context.setVariable("ticket", ticket);
        context.setVariable("total", total);

        return templateEngine.process(VIEW_NAME, context);
    }
}
