package com.epam.sergeev.helpDesk.model.enums;

public enum UserRole {
    Employee,
    Manager,
    Engineer;
}
