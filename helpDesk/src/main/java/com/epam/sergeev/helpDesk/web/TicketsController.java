package com.epam.sergeev.helpDesk.web;

import com.epam.sergeev.helpDesk.service.ticket.TicketService;
import com.epam.sergeev.helpDesk.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class TicketsController {

    @Autowired
    TicketService ticketService;

    @Autowired
    UserService userService;

    @RequestMapping(value = {"/AllTickets"}, method = RequestMethod.GET)
    public ModelAndView tickets() {
        ModelAndView mav = new ModelAndView("tickets");
        UserDetails ud = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        mav.addObject("user", ud.getUsername());
        mav.addObject("tickets", ticketService.getAll());
        return mav;
    }

    @ModelAttribute("userEmail")
    public String getUserEmail() {
        return userService.getUserEmail();
    }
}
