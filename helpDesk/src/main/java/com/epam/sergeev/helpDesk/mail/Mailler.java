package com.epam.sergeev.helpDesk.mail;

import java.util.List;
import javafx.util.Pair;

public interface Mailler {

    public void sendMail(String name, List<Pair> byTicket, double total);
}
