package com.epam.sergeev.helpDesk.service.ticket;

import com.epam.sergeev.helpDesk.model.Ticket;
import java.util.List;

public interface TicketService {

    void create(Ticket ticket);

    List<Ticket> getAll();

}
