package com.epam.sergeev.helpDesk.dao.ticket;

import com.epam.sergeev.helpDesk.dao.CRUDdaoImpl;
import com.epam.sergeev.helpDesk.model.Ticket;
import org.springframework.stereotype.Repository;

@Repository
public class TicketDaoImpl extends CRUDdaoImpl<Ticket> implements TicketDao {

    public TicketDaoImpl() {
        super(Ticket.class);
    }

}
