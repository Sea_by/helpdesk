package com.epam.sergeev.helpDesk.service.ticket;

import com.epam.sergeev.helpDesk.dao.ticket.TicketDao;
import com.epam.sergeev.helpDesk.model.Ticket;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class TicketServiceImpl implements TicketService {

    @Autowired
    TicketDao ticketDao;

    @Override
    public void create(Ticket ticket) {
        ticketDao.create(ticket);
    }

    @Override
    public List<Ticket> getAll() {
        return ticketDao.getAll();
    }

}
