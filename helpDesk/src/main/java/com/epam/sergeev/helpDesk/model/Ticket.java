package com.epam.sergeev.helpDesk.model;

import com.epam.sergeev.helpDesk.model.enums.State;
import com.epam.sergeev.helpDesk.model.enums.Urgency;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;

@Entity
public class Ticket implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TicketId")
    private int id;

    private String name;
    private String description;

    @Temporal(javax.persistence.TemporalType.DATE)
    private Date createdOn;

    @Temporal(javax.persistence.TemporalType.DATE)
    private Date desiredResolutionDate;

    @ManyToOne
    @JoinColumn(name = "AssigneeId")
    private User assignee;

    @ManyToOne
    @JoinColumn(name = "OwnerId")
    private User owner;

    private State state;

    @OneToOne
    @JoinColumn(name = "CategoryId")
    private Category category;

    private Urgency urgency;

    @ManyToOne
    @JoinColumn(name = "ApproverId")
    private User approver;

    public Ticket() {
    }

    public Ticket(int id, String name, String description) {
        this.id = id;
        this.description = description;
        this.name = name;
    }

    public Ticket(int id, String name, String description, Date createdOn, Date desiredResolutionDate, User assignee, User owner, State state, Category category, Urgency urgency, User approver) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.createdOn = createdOn;
        this.desiredResolutionDate = desiredResolutionDate;
        this.assignee = assignee;
        this.owner = owner;
        this.state = state;
        this.category = category;
        this.urgency = urgency;
        this.approver = approver;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public Date getDesiredResolutionDate() {
        return desiredResolutionDate;
    }

    public User getAssignee() {
        return assignee;
    }

    public User getOwner() {
        return owner;
    }

    public State getState() {
        return state;
    }

    public Category getCategory() {
        return category;
    }

    public Urgency getUrgency() {
        return urgency;
    }

    public User getApprover() {
        return approver;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public void setDesiredResolutionDate(Date desiredResolutionDate) {
        this.desiredResolutionDate = desiredResolutionDate;
    }

    public void setAssignee(User assignee) {
        this.assignee = assignee;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public void setState(State state) {
        this.state = state;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public void setUrgency(Urgency urgency) {
        this.urgency = urgency;
    }

    public void setApprover(User approver) {
        this.approver = approver;
    }

}
