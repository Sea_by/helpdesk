package com.epam.sergeev.helpDesk.web;

import com.epam.sergeev.helpDesk.service.user.UserService;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ErrorPageController {

    @Autowired
    private UserService userService;

    private static final String PATH = "/error";

    @RequestMapping(value = PATH)
    public void error(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.sendRedirect("/AllTickets");
    }

    @RequestMapping(value = "/403", method = RequestMethod.GET)
    public Model accessDenied(Model model) {
        return model.addAttribute("user", userService.getUserEmail());
    }
}
